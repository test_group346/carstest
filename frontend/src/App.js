import React from "react";
import Provider from "ra-data-json-server"
import {Admin, Resource, fetchUtils} from 'react-admin'
import authProvider from "./authProvider";
import CarIcon from "@mui/icons-material/CarRental"
import {CarsList, CarsCreate, CarsEdit} from "./cars";
import './App.css'


const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({Accept: 'application/json'})
    }
    options.headers.set(
        'Authorization',
        `Bearer ${localStorage.getItem('token')}`
    )
    return fetchUtils.fetchJson(url, options)
}

function App() {
    return (
        <Admin dataProvider={Provider('http://localhost:9900', httpClient)} authProvider={authProvider}>
            <Resource icon={CarIcon} name='cars' list={CarsList} edit={CarsEdit} create={CarsCreate}/>
        </Admin>
    );
}

export default App;
