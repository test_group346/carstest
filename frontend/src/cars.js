import React from 'react'
import {
    List, Datagrid, TextField,
    ChipField, DeleteButton, EditButton,
    required, Edit, SimpleForm,
    TextInput, NumberInput, Create,
    SelectInput
} from 'react-admin'


const comparators = [
    {id: 'gt', name: '>'},
    {id: 'ge', name: '>='},
    {id: 'eq', name: '='},
    {id: 'lt', name: '<'},
    {id: 'le', name: "<="}
]


const CarFilters = [
    <TextInput source='brand'/>,
    <TextInput source='model'/>,
    <SelectInput source='race_mode' choices={comparators}/>,
    <NumberInput source='race'/>,
    <SelectInput source='price_mode' choices={comparators}/>,
    <NumberInput source='price'/>,
]


export const CarsList = () => (
    <List filters={CarFilters}>
        <Datagrid>
            <TextField source="id" />
            <ChipField source="model" />
            <ChipField source="brand" />
            <TextField source="race" />
            <TextField source="price" />
            <EditButton/>
            <DeleteButton/>
        </Datagrid>
    </List>
);

const CarForm = () => {
    return (
        <div>
            <TextInput name='brand' source='brand' validate={required()}/>
            <TextInput name='model' source='model' validate={required()}/>
            <NumberInput name='race' source='race' validate={required()}/>
            <NumberInput name='price' source='price' validate={required()}/>
        </div>
    )
}


export const CarsEdit = () => {
    return (
        <Edit>
            <SimpleForm>
                <TextInput disabled name='id' source='id'/>
                <CarForm/>
            </SimpleForm>
        </Edit>
    )
}

export const CarsCreate = () => {
    return (
        <Create>
            <SimpleForm>
                <CarForm/>
            </SimpleForm>
        </Create>
    )
}
