FROM python:3.8.13-alpine3.16

WORKDIR /usr/src/app

RUN mkdir server
COPY ./server ./server
COPY run.py .
COPY reqs.txt .
RUN pip install --no-cache-dir -r reqs.txt
RUN apk add gcc musl-dev python3-dev libffi-dev openssl-dev
RUN pip install --no-cache-dir cryptography==2.1.4

CMD ["python", "run.py"]
