from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_restful import Api


app = Flask(__name__)
jwt = JWTManager(app)
api = Api(app)
CORS(app)
app.config.update(
    SECRET_KEY="A\xacE\x94\x04\x12\x93\xef\xa4\xea\xdd>\xff\t\x06\x00<\xb6J\xc6n\xba=\x02\xbb",
    SQLALCHEMY_DATABASE_URI="mysql+pymysql://root:1234@db/cars_app",
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
    DEBUG=True,
)


from server.resources.cars import Cars, CarModify
from server.resources.users import Users
from .models import db_init


api.add_resource(Cars, "/cars")
api.add_resource(CarModify, '/cars/<int:car_id>')
api.add_resource(Users, "/users")
db_init()
