from flask_restful import Resource, reqparse
from flask_jwt_extended import create_access_token
from ..common.utils import authenticate


"""
Url params parser when user try login in admin
"""
parser = reqparse.RequestParser()\
    .add_argument(
        'username', dest='username',
        type=str, location='json',
        required=True
    ).add_argument(
        'password', dest='password',
        type=str, location='json',
        required=True
    )


class Users(Resource):

    def post(self) -> dict:
        """
        Check user in db and
        if user exists, give him auth token
        :return:
        """
        args = parser.parse_args()
        user = authenticate(
            args['username'], args['password']
        )
        if user:
            token = create_access_token(identity={
                'role': 'admin',
            }, expires_delta=False)
            return {'token': token}
        return {
            'error': "Invalid username or password"
        }
