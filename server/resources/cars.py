from flask import make_response, Response
from flask_restful import Resource, reqparse, fields, marshal_with_field, marshal_with
from flask_jwt_extended import jwt_required
from typing import List
from ..models import Car, db
from ..common.utils import sorting, with_count


def combinator(mode: str):
    """
    Custom type checker for mode params
    :param mode: name of logical method
    :return:
    """
    try:
        return getattr(int, f"__{mode}__").__name__
    except AttributeError:
        raise TypeError


"""
Url params parser for car sorting and filtering 
"""
car_parser = reqparse.RequestParser() \
    .add_argument(
        "_start", dest='start',
        type=int, location='args'
    ).add_argument(
        '_order', dest='order',
        type=str, location='args'
    ).add_argument(
        '_sort', dest='sort',
        type=str, location='args'
    ).add_argument(
        '_end', dest='end',
        type=int, location='args'
    ).add_argument(
        "brand", dest='brand',
        type=str, location='args'
    ).add_argument(
        "model", dest='model',
        type=str, location='args'
    ).add_argument(
        "price", dest='price',
        type=int, location='args'
    ).add_argument(
        "price_mode", dest='price_mode',
        type=combinator, location='args'
    ).add_argument(
        'race', dest='race',
        type=int, location='args'
    ).add_argument(
        'race_mode', dest='race_mode',
        type=combinator, location='args'
    )

"""
Url params parser for update, create, get and delete a single car
"""
car_modify_parser = reqparse.RequestParser() \
    .add_argument(
        'brand', dest='brand',
        type=str, location='json',
    ).add_argument(
        'id', dest='id',
        type=int, location='json'
    ).add_argument(
        'model', dest='model',
        type=str, location='json'
    ).add_argument(
        'price', dest='price',
        type=int, location='json'
    ).add_argument(
        'race', dest='race',
        type=int, location='json'
    )

"""
Extracting fields for Car model
"""
car_fields = {
    'id': fields.Integer,
    'brand': fields.String,
    'model': fields.String,
    'race': fields.Integer,
    'price': fields.Integer,
}


class Cars(Resource):

    @jwt_required(locations=['headers'])
    @with_count(count_model=Car)
    @marshal_with_field(fields.List(fields.Nested(car_fields)))
    def get(self) -> List[Car]:
        """
        Parse available url params and sort model by
        this params
        :return:
        """
        return sorting(Car, **car_parser.parse_args())

    @marshal_with(car_fields)
    @jwt_required(locations=['headers'])
    def post(self) -> Car:
        """
        Evoked, when Car item must be created
        :return:
        """
        car = Car(**car_modify_parser.parse_args())
        db.session.add(car)
        db.session.commit()
        return car

    def options(self) -> Response:
        """
        Needs for inform server with available methods
        :return:
        """
        return make_response()


class CarModify(Resource):

    @jwt_required(locations=['headers'])
    @marshal_with(car_fields)
    def get(self, car_id: int) -> Car:
        """
        Get a single car object
        :param car_id:
        :return:
        """
        return Car.query.filter(Car.id == car_id).first()

    @jwt_required(locations=['headers'])
    @marshal_with(car_fields)
    def put(self, car_id: int) -> Car:
        """
        Evokes, when car must be updated
        :param car_id:
        :return:
        """
        car = Car.query\
            .filter(Car.id == car_id)
        car.update(car_modify_parser.parse_args())
        db.session.commit()
        return car

    @jwt_required(locations=['headers'])
    def delete(self, car_id: int) -> int:
        Car.query.filter(Car.id == car_id).delete()
        db.session.commit()
        return car_id

    def options(self, car_id: int) -> Response:
        """
        Needs for inform server with available methods
        :param car_id:
        :return:
        """
        return make_response()
