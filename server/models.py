from flask_sqlalchemy import SQLAlchemy
from .app import app


db = SQLAlchemy(app, engine_options={'pool_pre_ping': True})


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(40), unique=True, nullable=False)
    password = db.Column(db.String(40), nullable=False)


class Car(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    brand = db.Column(db.String(40), nullable=False)
    model = db.Column(db.String(40), unique=True, nullable=False)
    race = db.Column(db.Integer, default=0)
    price = db.Column(db.Integer, nullable=False)


def db_init():
    db.drop_all()
    db.create_all()
    db.session.commit()
    db.session.add(User(username="admin", password="admin"))
    db.session.add(Car(brand='Mercedes-Benz', model='A2', race=12340, price=2430))
    db.session.add(Car(brand='BMW', model='TR-2', race=20340, price=4000))
    db.session.add(Car(brand='Audi', model='FK32', race=15000, price=4200))
    db.session.add(Car(brand='BMW', model='IO-2', race=18020, price=5000))
    db.session.add(Car(brand='Reno', model='LISA32', race=12000, price=6000))
    db.session.add(Car(brand='Peugeot', model='5000', race=30120, price=5500))
    db.session.commit()
