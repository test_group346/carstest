import json
from functools import wraps
from typing import Union, List, Callable
from flask import make_response, Response
from server.models import User, db
import hmac
from sqlalchemy import asc, desc


"""
Help functions for `authenticate`
"""
str_to_bytes = lambda string: string.encode("utf-8") if isinstance(string, str) else string
safe_str_cmp = lambda str_a, str_b: hmac.compare_digest(str_to_bytes(str_a), str_to_bytes(str_b))


def sorting(
        sort_model: object,
        start: int,
        end: int,
        order: str,
        sort: str,
        brand: str = None,
        model: str = None,
        price: int = None,
        race: int = None,
        price_mode: str = None,
        race_mode: str = None
) -> List[db.Model]:
    """
    Sort a `sort_model` by passed params
    and return a list of `sort_model` or empty list

    :param race_mode: compare method for race
    :param price_mode: compare method for price
    :param race: by which race filtering
    :param price: by which price filtering
    :param model: by which model filtering
    :param brand: by which brand filtering
    :param sort_model: which model will be sorted
    :param start: from which item start
    :param end: in which item ends
    :param order: asc or desc
    :param sort: by which key are sorting
    :return:
    """
    if price_mode is None:
        price_mode = '__eq__'
    if race_mode is None:
        race_mode = '__eq__'
    
    return sort_model.query\
        .order_by((asc if order == 'ASC' else desc)(getattr(sort_model, sort))) \
        .filter(sort_model.brand.startswith(brand) if brand else True) \
        .filter(sort_model.model.startswith(model) if model else True) \
        .filter(getattr(sort_model.price, price_mode)(price) if price else True) \
        .filter(getattr(sort_model.race, race_mode)(race) if race else True)[start:end] or []


def authenticate(username: str, password: str) -> Union[User, None]:
    """
    Check user in db
    and compare passed user password with password in db

    :param username:
    :param password:
    :return:
    """
    user = User.query.filter(User.username == username).first()
    if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
        return user


def with_count(count_model: object) -> Callable:
    """
    Create Response and add to him additional count header

    :param count_model: from which model count will be taken
    :return:
    """
    def wrapper(fn) -> Callable:
        @wraps(fn)
        def argument_wrapper(*args, **kwargs) -> Response:
            response = make_response()
            response.headers.update({
                'Access-Control-Expose-Headers': 'X-Total-Count',
                'X-Total-Count': count_model.query.count()
            })
            response.data = json.dumps(fn(*args, **kwargs))
            return response
        return argument_wrapper
    return wrapper